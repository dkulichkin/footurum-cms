// Generated on 2015-02-10 using generator-angular-fullstack 2.0.13
'use strict';

module.exports = function (grunt) {

  require('dotenv').load({path: './.env'});

  // Load grunt tasks automatically, when needed
  require('jit-grunt')(grunt, {
    express: 'grunt-express-server',
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    cdnify: 'grunt-google-cdn',
    injector: 'grunt-asset-injector',
    ngconstant: 'grunt-ng-constant'
  });

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Define the configuration for all the tasks
  grunt.initConfig({

    ngconstant: {
      options: {
        name: 'config',
        constants: {
          ENV: process.env.NODE_ENV,
          ApiEndpoint: process.env.API_URI
        }
      },
      development: {
        options: {
          dest: 'client/app/config.js'
        }
      },
      alpha: {
        options: {
          dest: 'dist/public/app/config.js'
        }
      },
      beta: {
        options: {
          dest: 'dist/public/app/config.js'
        }
      },
      production: {
        options: {
          dest: 'dist/public/app/config.js'
        }
      }
    },

    // Project settings
    uglify: {
      options: {
        beautify: false,
        mangle: true
      }
    },

    pkg: grunt.file.readJSON('package.json'),

    express: {
      options: {
        port: process.env.SERVER_PORT || 80
      },
      development: {
        options: {
          script: 'server/app.js',
          debug: false
        }
      },
      alpha: {
        options: {
          script: 'dist/server/app.js',
          debug: false
        }
      },
      beta: {
        options: {
          script: 'dist/server/app.js',
          debug: false
        }
      },
      production: {
        options: {
          script: 'dist/server/app.js',
          debug: false
        }
      }
    },

    open: {
      server: {
        url: 'http://localhost:<%= express.options.port %>'
      }
    },

    watch: {
      injectJS: {
        files: [
          'client/app/**/*.js',
          '!client/app/**/*.spec.js',
          '!client/app/**/*.mock.js'
        ],
        tasks: ['injector:scripts']
      },
      injectCss: {
        files: [
          'client/app/**/*.css'
        ],
        tasks: ['injector:css']
      },
      injectSass: {
        files: [
          'client/app/**/*.{scss,sass}'],
        tasks: ['injector:sass']
      },
      sass: {
        files: [
          'client/app/**/*.{scss,sass}'],
        tasks: ['sass', 'autoprefixer']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        files: [
          '{.tmp,client}/app/**/*.css',
          '{.tmp,client}/app/**/*.html',
          '{.tmp,client}/app/**/*.js',
          '!{.tmp,client}app/**/*.spec.js',
          '!{.tmp,client}/app/**/*.mock.js',
          'client/assets/images/{,*//*}*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        options: {
          livereload: true
        }
      },
      express: {
        files: [
          'server/**/*.{js,json}'
        ],
        tasks: ['express:development', 'wait'],
        options: {
          livereload: true,
          nospawn: true //Without this option specified express won't be reloaded
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: 'client/.jshintrc',
        reporter: require('jshint-stylish')
      },
      server: {
        options: {
          jshintrc: 'server/.jshintrc'
        },
        src: [
          'server/**/*.js',
          '!server/**/*.spec.js'
        ]
      },
      serverTest: {
        options: {
          jshintrc: 'server/.jshintrc-spec'
        },
        src: ['server/**/*.spec.js']
      },
      all: [
        'client/app/**/*.js',
        '!client/app/**/*.spec.js',
        '!client/app/**/*.mock.js'
      ],
      test: {
        src: [
          'client/app/**/*.spec.js',
          'client/app/**/*.mock.js'
        ]
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            'dist/*.*',
            '!dist/.git*',
            '!dist/.openshift',
            '!dist/Procfile',
            'dist/public/*',
            'dist/server/*',
            '!dist/server/uploads'
          ]
        }]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/',
          src: '{,*/}*.css',
          dest: '.tmp/'
        }]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      target: {
        src: 'client/index.html',
        ignorePath: 'client/',
        exclude: [/bootstrap-sass-official/, /bootstrap.js/, '/json3/', '/es5-shim/', '/jquery/', /bootstrap.css/, /font-awesome.css/ ]
      }
    },

    // Renames files for browser caching purposes
    rev: {
      dist: {
        files: {
          src: [
            'dist/public/{,*/}*.js',
            'dist/public/{,*/}*.css',
            'dist/public/assets/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
            'dist/public/assets/fonts/*'
          ]
        }
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: ['client/index.html'],
      options: {
        dest: 'dist/public'
      }
    },

    // Performs rewrites based on rev and the useminPrepare configuration
    usemin: {
      html: ['dist/public/{,*/}*.html'],
      css: ['dist/public/{,*/}*.css'],
      js: ['dist/public/{,*/}*.js'],
      options: {
        assetsDirs: [
          'dist/public',
          'dist/public/assets/images',
          'dist/public/assets/fonts'
        ],
        // This is so we update image references in our ng-templates
        patterns: {
          js: [
            [/(assets\/images\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the JS to reference our revved images']
          ],
          css: [
            [/(assets\/images\/.*?\.(?:gif|jpeg|jpg|png|webp|svg))/gm, 'Update the CSS to reference our revved images'],
            [/(assets\/fonts\/.*?\.(?:eot|woff|ttf|svg))/gm, 'Update the CSS to reference our revved fonts']
          ]
        }
      }
    },

    // The following *-min tasks produce minified files in the dist folder
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: 'client/assets/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: 'dist/public/assets/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: 'client/assets/images',
          src: '{,*/}*.svg',
          dest: 'dist/public/assets/images'
        }]
      }
    },

    // Allow the use of non-minsafe AngularJS files. Automatically makes it
    // minsafe compatible so Uglify does not destroy the ng references
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat',
          src: '*/**.js',
          dest: '.tmp/concat'
        }]
      }
    },

    // Package all the html partials into a single javascript payload
    ngtemplates: {
      options: {
        module: 'footurumCms',
        htmlmin: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true
        },
        usemin: 'app/app.js'
      },
      main: {
        cwd: 'client',
        src: ['app/**/*.html'],
        dest: '.tmp/templates.js'
      },
      tmp: {
        cwd: '.tmp',
        src: ['app/**/*.html'],
        dest: '.tmp/tmp-templates.js'
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['dist/public/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'client',
          dest: 'dist/public',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'bower_components/**/*',
            'vendors/**/*',
            'assets/images/{,*/}*.{webp}',
            'assets/fonts/**/*',
            'index.html'
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: 'dist/public/assets/images',
          src: ['generated/*']
        }, {
          expand: true,
          dot: true,
          dest: 'dist',
          src: [
            '.env',
            'package.json',
            'bower.json',
            'server/**/*'
          ]
        }]
      },
      styles: {
        expand: true,
        cwd: 'client',
        dest: '.tmp/',
        src: ['app/**/*.css']
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'sass'
      ],
      test: [
        'sass'
      ],
      dist: [
        'sass',
        'imagemin',
        'svgmin'
      ]
    },

    // Compiles Sass to CSS
    sass: {
      server: {
        options: {
          loadPath: [
            'client/bower_components',
            'client/app'
          ],
          compass: false
        },
        files: {
          '.tmp/app/app.css' : 'client/app/app.scss'
        }
      }
    },

    injector: {

      // Inject application script files into index.html (doesn't include bower)
      scripts: {
        options: {
          transform: function(filePath) {
            filePath = filePath.replace('/client/', '');
            filePath = filePath.replace('/.tmp/', '');
            return '<script src="' + filePath + '"></script>';
          },
          starttag: '<!-- injector:js -->',
          endtag: '<!-- endinjector -->'
        },
        files: {
          'client/index.html': [
            [
              '{.tmp,client}/{vendors,app}/**/*.js',
              '!{.tmp,client}/{vendors,app}/**/*.spec.js',
              '!{.tmp,client}/{vendors,app}/**/*.mock.js'
            ]
          ]
        }
      },

      // Inject component scss into app.scss
      sass: {
        options: {
          transform: function(filePath) {
            filePath = filePath.replace('/client/app/', '');
            filePath = filePath.replace('/client/components/', '');
            return '@import \'' + filePath + '\';';
          },
          starttag: '// injector',
          endtag: '// endinjector'
        },
        files: {
          'client/app/app.scss': [
            'client/{app,components}/**/*.{scss,sass}',
            '!client/app/app.{scss,sass}'
          ]
        }
      },

      // Inject component css into index.html
      css: {
        options: {
          transform: function(filePath) {
            filePath = filePath.replace('/client/', '');
            filePath = filePath.replace('/.tmp/', '');
            return '<link rel="stylesheet" href="' + filePath + '">';
          },
          starttag: '<!-- injector:css -->',
          endtag: '<!-- endinjector -->'
        },
        files: {
          'client/index.html': [
            'client/app/**/*.css'
          ]
        }
      }
    }
  });

  // Used for delaying livereload until after server has restarted
  grunt.registerTask('wait', function () {
    grunt.log.ok('Waiting for server reload...');

    var done = this.async();

    setTimeout(function () {
      grunt.log.writeln('Done waiting!');
      done();
    }, 1500);
  });

  grunt.registerTask('express-keepalive', 'Keep grunt running', function() {
    this.async();
  });

  grunt.registerTask('serve', function (target) {

    if (target === 'dist')
    {
      return grunt.task.run(['build', 'express:' + process.env.NODE_ENV, 'wait', 'open', 'express-keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'ngconstant:' + process.env.NODE_ENV,
      'injector:sass',
      'concurrent:server',
      'injector',
      'wiredep',
      'autoprefixer',
      'express:' + process.env.NODE_ENV,
      'wait',
      'open',
      'watch'
    ]);
  });

  grunt.registerTask('build', [
    'clean:dist',
    'ngconstant:' + process.env.NODE_ENV,
    'injector:sass',
    'concurrent:dist',
    'injector',
    'wiredep',
    'useminPrepare',
    'autoprefixer',
    'ngtemplates',
    'concat',
    'ngAnnotate',
    'copy:dist',
    'cdnify',
    'cssmin',
    'uglify',
    'rev',
    'usemin'
  ]);

};
