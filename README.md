#Starting the service

Production

```
npm start
```

Development

```
grunt serve
```

#Building:

First specify a proper environment in .env file, then

```
grunt build
```

Serving built version locally

```
grunt serve:dist
```
