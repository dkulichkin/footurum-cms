/**
 * Main application file
 */

'use strict';

// Loading .env settings
require('dotenv').load({path: __dirname + '/../.env'});

var debug = require('./components/debug');
var log = debug.app.log;
var error = debug.app.error;

var env = process.env.NODE_ENV;
var path = require('path');
var baseAuth = require('./components/basic-auth')(process.env.BASIC_AUTH_USER, process.env.BASIC_AUTH_PASSWORD);
var express = require('express');
var compression = require('compression');
var mongoUImiddleware = require('mongo-express/middleware');
var mongoUIconfig = require('./components/mongo-config');
var app = express();
var server = require('http').createServer(app);

app.use(compression({level:9}));

if (env !== 'development')
{
	app.use(express.static('./dist/public'));
	app.set('appPath', __dirname + '/../public');
}
else
{
	app.use(express.static('./.tmp'));
	app.use(express.static('./client'));
	app.set('appPath', __dirname + '/../client');
}

app.set('read_only', mongoUIconfig.options.readOnly || false);

app.use('/admin', baseAuth);

app.use('/admin/guts', mongoUImiddleware(mongoUIconfig));

app.use(function(req, res)
{
	res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
});

process.on('uncaughtException', error);

server.listen(process.env.SERVER_PORT, function(err)
{
	err ? error(err) : log('listening on port ' + process.env.SERVER_PORT + ' in ' + env + ' environment');
});
