'use strict';

var debug = require('debug');
var rollbar = require("rollbar");
var logError = function(type)
{
	return process.env.NODE_ENV === 'development'|| process.env.NODE_ENV === 'alpha'
		? function(e) { debug(type).call(null, e) }
		: function(e, request) { rollbar.handleError.apply(null, arguments) }
};

rollbar.init(process.env.ROLLBAR_KEY, {
	environment: process.env.NODE_ENV,
	verbose: false
});

module.exports = {
	app: {
		log: debug('app'),
		error: logError('app:error')
	}
};
