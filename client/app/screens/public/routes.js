'use strict';

angular.module('footurumCms')
  .config(['$stateProvider', function($stateProvider) {

    $stateProvider
      .state('root.public.login', {
        url: '/',
        views: {
          'content@root': {
            templateUrl: 'app/screens/public/login/login.tpl.html',
            controller: 'LoginCtrl'
          }
        },
        onEnter: ['$state', 'Auth', function($state, Auth) {
          // Don't step in if logged already
          return Auth.isLoggedInAsync(function(loggedIn)
          {
            if (loggedIn) Auth.successfulLoginRedirect();
          })
        }]
      });
  }]);
