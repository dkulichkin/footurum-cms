'use strict';

angular.module('footurumCms')
  .controller('LoginCtrl', ['$scope', 'Auth', 'FlashMessage',
    function ($scope, Auth, FlashMessage) {

      $scope.user = {};
      $scope.error = FlashMessage.getMessage() || null;

      $scope.login = function(loginForm) {
        if(loginForm.$valid) {
          Auth.login({
            email: $scope.user.email,
            password: $scope.user.password
          })
          .then( function() {
            Auth.successfulLoginRedirect();
          })
          .catch( function(err) {
            $scope.error = err.data && err.data.error || err.message || angular.toJson(err);
          });
        }
      };

    }]);
