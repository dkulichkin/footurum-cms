'use strict';

angular.module('footurumCms')
  .config(['$stateProvider', 'rolesMapServiceProvider', function ($stateProvider, rolesMapServiceProvider) {

    var accessLevels = rolesMapServiceProvider.getAccessLevels();

    // Defining abstract routes
    $stateProvider
      .state('root', {
        abstract: true,
        views: {
			'layout@': {
			templateUrl: 'app/screens/base/layout.tpl.html'
          },
          'header@root': {
            templateUrl: 'app/screens/base/header.tpl.html'
          },
          'footer@root': {
            templateUrl: 'app/screens/base/footer.tpl.html'
          }
        }
      })
      .state('root.public', {
        abstract: true,
        views: {},
        data: {
          access: accessLevels.public
        }
      })
      .state('root.manager', {
        abstract: true,
        views: {
          'header@root': {
            templateUrl: 'app/components/ui/header/header.tpl.html',
            controller: 'HeaderCtrl'
          }
        },
        resolve: {
          UserProfileModel: ['Auth', function(Auth) {
            return Auth.getCurrentUser();
          }]
        },
        data: {
          access: accessLevels.manager
        }
      })
      .state('root.admin', {
        abstract: true,
        url: '/admin',
        views: {
          'header@root': {
            templateUrl: 'app/components/ui/header/header.tpl.html',
            controller: 'HeaderCtrl'
          }
        },
		resolve: {
			UserProfileModel: ['Auth', function(Auth) {
				return Auth.getCurrentUser();
			}]
		},
        data: {
          access: accessLevels.admin
        }
      });

  }]);
