'use strict';

angular.module('footurumCms')
	.factory('FootsGrid', ['Foot', function(Foot)
	{
		return function(Skill)
		{
			var _skill = angular.copy(Skill);
			var _grid = _skill.getFootsArray();

			_grid.addFoot = function(groupIndex, foot)
			{
				_grid[groupIndex].push(foot);
			};

			_grid.extract = function()
			{
				return _grid.map(function(footsArr)
				{
					return {
						_id: footsArr.group_id,
						group_name: footsArr.group_name,
						foots: Array.prototype.slice.call(footsArr)
					}
				});
			};

			_grid.update = function(_foot, idx, groupIdx) {
				return _grid[groupIdx].splice(idx, 1, _foot);
			};

			_grid.remove = function(idx, groupIdx)
			{
				return _grid[groupIdx].splice(idx, 1);
			};

			_grid.addGroup = function(group) {
				var newGroup = [];

				newGroup.group_id = group._id;
				newGroup.group_name = group.group_name;

				_grid.push(newGroup);
			};

			_grid.removeGroup = function(index)
			{
				return _grid.splice(index, 1);
			};

			return _grid;

		}

	}]);
