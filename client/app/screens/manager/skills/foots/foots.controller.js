'use strict';

angular.module('footurumCms')
	.controller('FootsCtrl', ['$scope', '$mdToast', 'SkillItem', 'FootsGrid', 'Modal', 'Foot',
	function($scope, $mdToast, SkillItem, FootsGrid, Modal, Foot)
	{
		$scope.language = SkillItem.languages && SkillItem.languages[0] || 'en';
		$scope.skill = SkillItem;

		$scope.setLang = function(_lang)
		{
			$scope.language = _lang;
		};

		$scope.languages = {
			en: $scope.skill.languages.indexOf('en') !== -1,
			ru: $scope.skill.languages.indexOf('ru') !== -1,
			ua: $scope.skill.languages.indexOf('ua') !== -1
		};

		$scope.footsGrid = FootsGrid(SkillItem);

		$scope.updateSkill = function()
		{
			$scope.skill.foot_groups = $scope.footsGrid.extract();

			$scope.skill.$update()
				.then(function()
				{
					$mdToast.show(
						$mdToast.simple()
							.content('Skill is updated')
							.position('top right')
							.hideDelay(3000)
					);

				})
				.catch(function(response) {
					Modal.showAlertModal('Error updating skill',
						response.data && response.data.error || angular.toJson(response)
					);
				});
		};

		$scope.reorder = function($item, $partFrom, $partTo, $indexFrom, $indexTo)
		{
			$scope.updateSkill();
		};

		$scope.addFoot = function(group_id, idx)
		{
			var newFoot = new Foot({
				skill_id: SkillItem._id
			});

			Modal.showModal({
				templateUrl: 'app/screens/manager/skills/foots/templates/form.foot.tpl.html',
				data: _.assign(newFoot, {
					languages: $scope.languages,
					action: 'add',
					selectedLanguage: $scope.language
				})
			})
			.then(function(foot)
			{
				return foot.$save();
			})
			.then(function(foot)
			{
				$scope.footsGrid.addFoot(idx, foot);
				$scope.updateSkill(); 
			})
			.catch(function(response)
			{
				_.isObject(response) && Modal.showAlertModal('Error creating foot', response.data && response.data.error || angular.toJson(response));
			});
		};

		$scope.deleteFoot = function(_foot, idx, groupIdx)
		{
			Modal.showConfirmationModal({
				title: [ 'Delete foot "', _foot.name[$scope.language] ,'" ?' ].join(' ')
			})
				.then(function()
				{
					return Foot.delete({id: _foot._id}).$promise;
				})
				.then(function()
				{
					$scope.footsGrid.remove(idx, groupIdx);
					$scope.updateSkill();
				})
				.catch(function(response)
				{
					_.isObject(response) && Modal.showAlertModal('Error deleting foot', response.data && response.data.error || angular.toJson(response));
				});
		};

		$scope.editFoot = function(_foot, idx, groupIdx)
		{
			Foot.get({id: _foot._id}).$promise
				.then(function(foot)
				{
					return Modal.showModal({
						templateUrl: 'app/screens/manager/skills/foots/templates/form.foot.tpl.html',
						data: _.assign(foot, {
							languages: $scope.languages,
							action: 'update',
							selectedLanguage: $scope.language
						})
					});
				})
				.then(function(foot)
				{
					return foot.$update();
				})
				.then(function(foot)
				{
					$scope.footsGrid.update(foot, idx, groupIdx);
					$scope.updateSkill();
				})
				.catch(function(response)
				{
					_.isObject(response) && Modal.showAlertModal('Error updating foot', response.data && response.data.error || angular.toJson(response));
				});
		};

		// TODO: server - side group save
		$scope.addGroup = function()
		{
			var groupsNum = $scope.skill.foot_groups.length + 1;

			Modal.showModal({
				templateUrl: 'app/screens/manager/skills/foots/templates/addgroup.tpl.html',
				data: {
					name: { en: 'Week ' + groupsNum, ru: 'Неделя ' + groupsNum, ua: 'Тиждень ' + groupsNum },
					languages_active: $scope.languages
				}
			})
			.then(function(data)
			{
				$scope.skill.foot_groups.push({group_name: data.name, foots: []});
				return $scope.skill.$update();
			})
			.then(function()
			{
				$scope.footsGrid.addGroup($scope.skill.foot_groups[ $scope.skill.foot_groups.length - 1 ]);

				$mdToast.show(
					$mdToast.simple()
						.content('Skill is updated')
						.position('top right')
						.hideDelay(3000)
				);
			})
			.catch(function(response){
					_.isObject(response) && Modal.showAlertModal('Error updating foot', response.data && response.data.error || angular.toJson(response));
			})
		};

		$scope.deleteGroup = function(grop, index)
		{
			if ($scope.skill.foot_groups[index].foots && $scope.skill.foot_groups[index].foots.length)
			{
				return Modal.showAlertModal('Group can not be deleted', 'Please remove foots from the group firstly');
			}

			Modal.showConfirmationModal({
				title: 'Delete foot group?'
			})
				.then(function()
				{
					$scope.skill.foot_groups.splice(index, 1);
					return $scope.skill.$update();
				})
				.then(function()
				{
					$scope.footsGrid.removeGroup(index);
					$mdToast.show(
						$mdToast.simple()
							.content('Skill is updated')
							.position('top right')
							.hideDelay(3000)
					);
				});
		};

	}]);
