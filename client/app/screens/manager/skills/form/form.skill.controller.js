'use strict';

angular.module('footurumCms')
	.controller('FormSkillCtrl', ['$scope', '$state', '$mdToast', '$timeout', 'Modal', 'Skill', 'CategoriesCollection', 'UserProfile', 'SkillItem', 'Action',
	function ($scope, $state, $mdToast, $timeout, Modal, Skill, CategoriesCollection, UserProfile, SkillItem, Action)
	{
		var langsArr = ['en', 'ru', 'ua'];

		$scope.action = Action;
		$scope.user = UserProfile;
		$scope.categories = CategoriesCollection;
		$scope.skill = SkillItem;

		// TODO: implement language control directive working directly with a skill model
		$scope.languages = {
			en: $scope.skill.languages.indexOf('en') !== -1,
			ru: $scope.skill.languages.indexOf('ru') !== -1,
			ua: $scope.skill.languages.indexOf('ua') !== -1
		};

		$scope.initialLanguages = Action === 'update' ? angular.copy($scope.languages) : { en: false, ru: false, ua: false };

		$scope.goal = {};

		$scope.maxArrLength = function(field)
		{
			var l1 = $scope.skill[field] && $scope.skill[field].en && $scope.skill[field].en.length || 0,
					l2 = $scope.skill[field] && $scope.skill[field].ru && $scope.skill[field].ru.length || 0,
					l3 = $scope.skill[field] && $scope.skill[field].ua && $scope.skill[field].ua.length || 0
			;

			return new Array(Math.max(l1, l2, l3));
		};

		$scope.checkLanguages = function(lang)
		{
			$timeout(function()
			{
				var langsActive = [];

				if ($scope.languages.en) langsActive.push('en');
				if ($scope.languages.ru) langsActive.push('ru');
				if ($scope.languages.ua) langsActive.push('ua');

				if (!$scope.languages.en && !$scope.languages.ua && !$scope.languages.ru)
				{
					$scope.languages[lang] = true;
				}

				// To not hide an active choosen description or advices...
				if (!$scope.languages[lang])
				{
					if ($scope.description.active === lang) $scope.description.active = langsActive[0];
					if ($scope.advice.active === lang) $scope.advice.active = langsActive[0];
				}
			});
		};

		$scope.addGoal = function()
		{
			if (!$scope.goal.name.en && $scope.languages.en || !$scope.goal.name.ru && $scope.languages.ru || !$scope.goal.name.ua && $scope.languages.ua)
			{
				return Modal.showAlertModal(null, 'Please fill in all values for goals');
			}

			$scope.skill.goals = $scope.skill.goals || {};

			langsArr.forEach(function(_lang)
			{
				if (!$scope.goal.name[_lang]) return;
				$scope.skill.goals[_lang] = $scope.skill.goals[_lang] || [];
				$scope.skill.goals[_lang].push( angular.copy($scope.goal.name[_lang]) );
			});

			$scope.goal = {};
		};

		$scope.removeGoal = function(idx)
		{
			langsArr.forEach(function(_lang)
			{
				$scope.skill.goals[_lang] && $scope.skill.goals[_lang].splice(idx, 1);
			})
		};

		$scope.description = {
			active: $scope.skill.languages[0] || 'en',
			toggle: function(lang) {
				$scope.description.active = lang;
			}
		};

		$scope.advice = {
			active: $scope.skill.languages[0] || 'en',
			text: {en: '', ru: '', ua: ''},
			toggle: function(lang)
			{
				$scope.advice.active = lang;
			},
			add: function()
			{
				if (!$scope.advice.text.en && $scope.languages.en || !$scope.advice.text.ru && $scope.languages.ru || !$scope.advice.text.ua && $scope.languages.ua)
				{
					return Modal.showAlertModal(null, 'Please fill in all values for advice');
				}

				$scope.skill.expert_advices = $scope.skill.expert_advices || {};

				langsArr.forEach(function(_lang)
				{
					$scope.skill.expert_advices[_lang] = $scope.skill.expert_advices[_lang] || [];
					$scope.skill.expert_advices[_lang].push( angular.copy($scope.advice.text[_lang]) );
				});

				$scope.advice.text = {en: '', ru: '', ua: ''};
			},
			remove: function(idx)
			{
				langsArr.forEach(function(_lang)
				{
					$scope.skill.expert_advices[_lang] && $scope.skill.expert_advices[_lang].splice(idx, 1);
				});
			}
		};

		$scope.submit = function(isFormValid)
		{
			var unsetLanguages;

			if (!isFormValid) return;

			// populating result languages been selected
			$scope.skill.languages = [];
			_.each($scope.languages, function(val, lang)
			{
				if ($scope.languages[lang]) $scope.skill.languages.push(lang);
			});

			if (!$scope.skill.languages.length) return Modal.showAlertModal(null, 'Select at least one language');

			unsetLanguages = _.difference(langsArr, $scope.skill.languages);

			$scope.skill.languages.forEach(function(_lang)
			{
				$scope.skill.tags[_lang] = $scope.skill.tags[_lang]
					&& $scope.skill.tags[_lang].toString().split(',').filter(function(tagString)
					{
						return tagString.trim();
					}) || [];
			});

			unsetLanguages.forEach(function(langToUnset)
			{
				['name', 'description', 'goals', 'expert_advices', 'tags'].forEach(function(attr)
				{
					delete $scope.skill[attr][langToUnset];
				});
			});

			if (Action === 'create')
			{
				$scope.skill.$save()
					.then(function()
					{
						$mdToast.show(
							$mdToast.simple()
								.content('Skill is created, you can edit and add foots and image now, please don\'t forget to activate the skill after you are done')
								.position('top right')
								.hideDelay(3000)
						);

						$state.go('root.manager.skills.edit', { id: $scope.skill._id });
					})
					.catch(function(response)
					{
						Modal.showAlertModal('Error creating skill', response.data && response.data.error || angular.toJson(response));
					});

			}
			else if (Action === 'update')
			{
				$scope.skill.$update()
					.then(function()
					{
						$mdToast.show(
							$mdToast.simple()
								.content('Skill is updated')
								.position('top right')
								.hideDelay(3000)
						);

						$state.go('root.manager.skills', {}, { reload: true });
					})
					.catch(function(response)
					{
						Modal.showAlertModal('Error updating skill', response.data && response.data.error || angular.toJson(response));
					});
			}

		};

		$scope.reset = function()
		{
			$scope.skill = {};
			$scope.addform.$setUntouched();
		};

		$scope.activateValidation = function()
		{
			/*if ($scope.skill.active && !$scope.skill.foot_groups || !$scope.skill.foot_groups.length)
			{
				Modal.showAlertModal('Error activating skill', 'You should have some foots inside');
				$scope.skill.active = false;
			}
			else
			{
				$mdToast.show(
					$mdToast.simple()
						.content('Skill is not active yet - press Update button to finish')
						.position('top right')
						.hideDelay(3000)
				);
			}*/

			// Temporary to allow adding of skill headers on beta
			$mdToast.show(
				$mdToast.simple()
					.content('Skill is not active yet - press Update button to finish')
					.position('top right')
					.hideDelay(3000)
			);
		};

	}]);
