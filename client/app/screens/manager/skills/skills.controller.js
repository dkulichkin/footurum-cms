'use strict';

angular.module('footurumCms')
	.controller('SkillsCtrl', ['$state', '$filter', '$scope', 'Modal', 'Skill', 'SkillsCollection', 'CategoriesCollection',
		function ($state, $filter, $scope, Modal, Skill, SkillsCollection, CategoriesCollection)
		{
			var _language = 'en';

			// compile regexp outside filter func
			var _searchRegExp;

			$scope.grid = {
				filter: {
					options: {
						debounce: 500
					},
					predicate: '',
					filterFunction: function(element)
					{
						if (!_searchRegExp) return true;

						return ( element.name.search(_searchRegExp) !== -1 )
							|| ( element.category.search(_searchRegExp) !== -1 )
							|| ( element.description.search(_searchRegExp) !== -1 )
							|| ( element.tags.join(' ').search(_searchRegExp) !== -1 );
					}
				},
				items: $filter('reduceByLanguage')(angular.copy(SkillsCollection), _language, CategoriesCollection),
				categories: CategoriesCollection,
				selected: [],
				order: 'name',
				language: _language,
				limit: 5,
				page: 1
			};

			$scope.$watch('grid.filter.predicate', function(newVal) {
				_searchRegExp = new RegExp(newVal.toLowerCase(), 'i');
			});

			$scope.setLang = function(_lang) {
				$scope.grid.language = _lang;
				$scope.grid.items = $filter('reduceByLanguage')(angular.copy(SkillsCollection), _lang, CategoriesCollection);
			}

			$scope.onOrderChange = function (order) {
				$scope.$apply(function()
				{
					$scope.grid.order = order;
				});
			};

			$scope.onPaginationChange = function (page, limit)
			{
				$scope.$apply(function()
				{
					$scope.grid.page = page || 1;
					$scope.grid.limit = limit || 5;
				});
			};

			$scope.deleteSkill = function()
			{
				Modal.showConfirmationModal({
					title: ['Delete', $scope.grid.selected.length > 1 ? $scope.grid.selected.length + ' skills?' : 'skill?'].join(' ')
				})
					.then(function()
					{
						$scope.grid.selected.forEach(function(_skill)
						{
							var id = _skill._id;
							Skill.delete({id: id}).$promise
								.then(function()
								{
									// ToDO: fix bug of grid updating
									_.remove($scope.grid.items, {_id: id });
								}).catch(function(response)
								{
									Modal.showAlertModal('Error deleting skill', response.data && response.data.error || angular.toJson(response));
								});
						});
						$scope.grid.selected = [];
					});
			};

			$scope.editSkill = function() {
				$scope.grid.selected[0] && $scope.grid.selected[0]._id
				&& $state.go('root.manager.skills.edit', {id: $scope.grid.selected[0]._id});
			}

			$scope.editSkillFoots = function() {
				$scope.grid.selected[0] && $scope.grid.selected[0]._id
				&& $state.go('root.manager.skills.edit.foots', {id: $scope.grid.selected[0]._id});
			}

		}
	]);
