'use strict';

angular.module('footurumCms')
	.config(['$stateProvider', function($stateProvider) {

		$stateProvider
			.state('root.manager.skills', {
				url: '/skills',
				views: {
					'content@root': {
						templateUrl: 'app/screens/manager/skills/layout.tpl.html',
						controller: 'SkillsCtrl'
					}
				},
				resolve: {
					SkillsCollection: ['Skill', function(Skill) {
						return Skill.query().$promise;
					}],
					CategoriesCollection: ['SkillCategory', function(SkillCategory) {
						return SkillCategory.query().$promise;
					}]
				}
			})
			.state('root.manager.skills.add', {
				url: '/add',
				views: {
					'content@root': {
						templateUrl: 'app/screens/manager/skills/form/form.skill.tpl.html',
						controller: 'FormSkillCtrl'
					}
				},
				resolve: {
					CategoriesCollection: ['SkillCategory', function(SkillCategory) {
						return SkillCategory.get().$promise;
					}],
					UserProfile: ['Auth', function(Auth) {
						return Auth.getCurrentUser();
					}],
					SkillItem: ['Skill', function(Skill) {
						return new Skill({
							languages: ['en', 'ru'],
							goals: { en: [], ru: [], ua: []},
							expert_advices: { en: [], ru: [], ua: [] },
							tags: { en: [], ru: [], ua: [] },
							active: false
						});
					}],
					Action: function() {
						return 'create';
					}
				}
			})
			.state('root.manager.skills.edit', {
				url: '/:id',
				views: {
					'content@root': {
						templateUrl: 'app/screens/manager/skills/form/form.skill.tpl.html',
						controller: 'FormSkillCtrl'
					}
				},
				resolve: {
					CategoriesCollection: ['SkillCategory', function(SkillCategory) {
						return SkillCategory.get().$promise;
					}],
					UserProfile: ['Auth', function(Auth) {
						return Auth.getCurrentUser();
					}],
					SkillItem: ['Skill', '$stateParams', function(Skill, $stateParams) {
						return Skill.get({ id: $stateParams.id }).$promise;
					}],
					Action: function() {
						return 'update';
					}
				}
			})
			.state('root.manager.skills.edit.foots', {
				url: '/foots',
				views: {
					'content@root': {
						templateUrl: 'app/screens/manager/skills/foots/templates/foots.tpl.html',
						controller: 'FootsCtrl'
					}
				},
				resolve: {
					CategoriesCollection: ['SkillCategory', function(SkillCategory) {
						return SkillCategory.get().$promise;
					}],
					UserProfile: ['Auth', function(Auth) {
						return Auth.getCurrentUser();
					}],
					SkillItem: ['$stateParams', 'Skill', function($stateParams, Skill) {
						return Skill.get( {id: $stateParams.id} ).$promise;
					}]
				}
			})
	}]);
