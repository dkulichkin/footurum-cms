'use strict';

angular.module('footurumCms')
	.controller('UsersCtrl', ['$scope', 'Modal', 'UsersCollection', 'User',
		function ($scope, Modal, UsersCollection, User)
		{
			$scope.grid = {
				filter: {
					options: {
						debounce: 500
					},
					predicate: '',
					filterFunction: function(element)
					{
						return (element.first_name && element.first_name.search($scope.grid.filter.predicate) !== -1)
							|| (element.last_name && element.last_name.search($scope.grid.filter.predicate) !== -1)
							|| (element.email && element.email.search($scope.grid.filter.predicate) !== -1)
							|| element.role.search($scope.grid.filter.predicate) !== -1
							|| element.language.search($scope.grid.filter.predicate) !== -1
								? true : false;
					}
				},
				items: UsersCollection,
				selected: [],
				order: 'first_name',
				limit: 5,
				page: 1
			};

			$scope.onOrderChange = function (order) {
				$scope.$apply(function()
				{
					$scope.grid.order = order;
				});
			};

			$scope.onPaginationChange = function (page, limit)
			{
				$scope.$apply(function()
				{
					$scope.grid.page = page || 1;
					$scope.grid.limit = limit || 5;
				});
			};

			$scope.addUser = function()
			{
				Modal.showModal({
					templateUrl: 'app/screens/admin/users/add.user.tpl.html'
				})
					.then(function(user)
					{
						return User.save(user).$promise
							.then(function(newUser)
							{
								$scope.grid.items.push(newUser);
							})
							.catch(function(response)
							{
								Modal.showAlertModal('Error creating user', response.data && response.data.error || angular.toJson(response));
							});
					});
			};

			$scope.deleteUser = function()
			{
				Modal.showConfirmationModal({
					title: ['Delete', $scope.grid.selected.length > 1 ? $scope.grid.selected.length + ' users?' : 'user?'].join(' ')
				})
					.then(function()
					{
						$scope.grid.selected.forEach(function(user)
						{
							User.delete({id: user._id}).$promise
								.then(function()
								{
									_.remove($scope.grid.items, {_id: user._id});
								});
						});
						$scope.grid.selected = [];
					});
			};

		}
	]);
