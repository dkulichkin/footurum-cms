'use strict';

angular.module('footurumCms')
	.config(['$stateProvider', function($stateProvider) {

		$stateProvider
			.state('root.admin.users', {
				url: '/users',
				views: {
					'content@root': {
						templateUrl: 'app/screens/admin/users/layout.tpl.html',
						controller: 'UsersCtrl'
					}
				},
				resolve: {
					UsersCollection: ['User', function(User) {
						return User.query().$promise;
					}]
				}
			});
	}]);
