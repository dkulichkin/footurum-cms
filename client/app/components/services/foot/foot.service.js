'use strict';

angular.module('footurumCms')
	.service('Foot', ['$resource', 'ApiEndpoint', function($resource, ApiEndpoint)
	{
		function formDataObject (data)
		{
			var formData = new FormData(),
					model = angular.copy(data)
			;

			// image file is handled separately and to stay pure File obj should be considered form original
			if (data.image && (data.image instanceof File))
			{
				formData.append("image", data.image);
				delete model.image;
			}

			formData.append("model", angular.toJson(model));
			return formData;
		}

		return $resource(ApiEndpoint + '/foots/:id/:controller', { id: '@_id', controller: '@_controller' }, {
			get: {
				isArray: false
			},
			save: {
				method: 'POST',
				transformRequest: formDataObject,
				headers: {'Content-Type': undefined}
			},
			update: {
				method: 'PUT',
				transformRequest: formDataObject,
				headers: {'Content-Type': undefined}
			}
		});
	}]);
