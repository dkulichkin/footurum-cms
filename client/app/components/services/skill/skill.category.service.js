'use strict';

angular.module('footurumCms')
	.factory('SkillCategory', ['$resource', '$q', 'ApiEndpoint', function ($resource, $q, ApiEndpoint)
		{

			var _categoriesCache = null;

			return $resource(ApiEndpoint + '/skillcategories/:id/', { id: '@id' },
			{
				query:
				{
					method: 'GET',
					cache: true,
					isArray: false,
					transformResponse: function(resp)
					{

						if (_categoriesCache)
						{
							return _categoriesCache;
						}

						var json = angular.fromJson(resp),
							ret = {};

						json.forEach(function(_cat)
						{
							ret[_cat._id] = _cat.name;
						});

						_categoriesCache = ret;

						return ret;
					}
				},
				get:
				{
					method: 'GET',
					cache: true,
					isArray: true
				}
			});
		}
	]);
