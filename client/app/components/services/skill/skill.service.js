'use strict';

angular.module('footurumCms')
	.factory('Skill', ['$resource', '$q', 'ApiEndpoint', 'Foot', function ($resource, $q, ApiEndpoint, Foot)
	{
		var _extra = {
			getFootsArray: function()
			{
				return this.foot_groups.reduce(function(_groups, _group, i)
				{
					_groups[i] = _group.foots.map(function(_foot)
					{
						return new Foot(_foot);
					});

					_groups[i].group_id = _group._id;
					_groups[i].group_name = _group.group_name;

					return _groups;

				}, []);
			},
			addFoot: function(idx, footObjId)
			{
				this.foot_groups[idx].push(footObjId);
				return this.$update();
			},
			addGroup: function(group)
			{
				this.foot_groups = this.foot_groups || [];
				this.foot_groups.push(group);
				return this.$update();
			}
		};

		function formDataObject (data)
		{
			var formData = new FormData(),
					model = angular.copy(data)
			;

			if (model.foot_groups && model.foot_groups.length)
			{
				model.foot_groups = model.foot_groups.map(function(group)
				{
					var _names = {};

					model.languages.forEach(function(lang)
					{
						_names[lang] = group.group_name[lang];
					});

					return {
						_id: group._id,
						group_name: _names,
						foots: _.isArray(group.foots) && _.isObject(group.foots[0]) ? _.pluck(group.foots, '_id') : group.foots
					};
				});
			}

			// image file is handled separately and to stay pure File obj should be considered form original
			if (data.image && (data.image instanceof File))
			{
				formData.append("image", data.image);
				delete model.image;
			}

			// deleting unnecessary fields here...
			delete model.num_followers;

			formData.append("model", angular.toJson(model));
			return formData;
		}

		var _skill = $resource(ApiEndpoint + '/skills/:id/:controller', { id: '@_id', controller: '@_controller' },
		{
			get: {
				isArray: false,
				interceptor: {
					response: function(response)
					{
						return $q.resolve( angular.extend(response.resource, _extra) );
					}
				}
			},
			save: {
				method: 'POST',
				transformRequest: formDataObject,
				headers: {'Content-Type': undefined}
			},
			update: {
				method: 'PUT',
				transformRequest: formDataObject,
				headers: {'Content-Type': undefined}
			}
		});

		return _skill;
	}]);
