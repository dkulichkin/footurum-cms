'use strict';

angular.module('footurumCms')
  .factory('authInterceptor', [
    '$rootScope',
		'$q',
		'localStorageService',
		'$location',
	function ($rootScope, $q, localStorageService, $location)
	{
    return {
      // Add authorization token to headers
      request: function (config)
			{
        config.headers = config.headers || {};

				if (localStorageService.get('token'))
				{
          config.headers.Authorization = 'Bearer ' + localStorageService.get('token');
        }

				return config;
      },

      // Intercept 401s and 403s and redirect to login
      responseError: function(response)
			{
        if(response.status === 401 || response.status === 403)
				{
          //to avoid circular dependencies injection using $location
          $location.path('/');
        }

        return $q.reject(response);
      }
    };
  }]);
