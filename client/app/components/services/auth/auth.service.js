'use strict';

angular.module('footurumCms')
  .factory('Auth', [
    '$http',
    'User',
    'localStorageService',
    '$q',
    '$state',
    'rolesMapService',
    'FlashMessage',
	'ApiEndpoint',
  function($http, User, localStorageService, $q, $state, rolesMapService, FlashMessage, ApiEndpoint)
	{

    var userRoles = rolesMapService.getUserRoles(),
        currentUser = localStorageService.get('token') ? User.get() : getDefaultGuestProps()
		;

    function getDefaultGuestProps()
    {
      return { role: 'public' };
    }

    /* Returning service's public API */

    return {

      authorize: function(accessLevel, role)
			{
        if(role === undefined)
				{
          role = userRoles[currentUser.role];
        }

        return accessLevel.bitMask & role.bitMask;
      },

      successfulLoginRedirect: function()
			{
        var doRedirect = function()
				{
					$state.go('root.manager.skills');
				};

        if(currentUser.hasOwnProperty('$promise'))
				{
          currentUser.$promise.then(function()
					{
            doRedirect();
          });
        }
        else
				{
          doRedirect();
        }
      },

      /**
       * Authenticate user and save token
       *
       * @param  {Object}   user     - login info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      login: function(user, callback)
			{
        var cb = callback || angular.noop;
        var deferred = $q.defer();

        $http.post(ApiEndpoint + '/signin/local',
				{
          email: user.email,
          password: user.password
        })
					.success(function(data)
					{
						localStorageService.set('token', data.token);
						currentUser = User.get();
						deferred.resolve(data);
						return cb();
					})
					.catch(function(err)
					{
						this.logout();
						deferred.reject(err);
						return cb(err);
					}.bind(this));

        return deferred.promise;
      },

      /**
       * Delete access token and user info
       */
      logout: function()
			{
        localStorageService.remove('token');
        currentUser = getDefaultGuestProps();
        $state.go('root.public.login');
      },

      /**
       * Gets all available info on authenticated user
       *
       * @return {Object} user or promise if still not resolved
       */
      getCurrentUser: function()
			{
        return currentUser.hasOwnProperty('$promise') ? currentUser.$promise : angular.copy(currentUser);
      },

      /**
       * Check if a user is logged in
       *
       * @return {Boolean}
       */
      isLoggedIn: function()
			{
        return currentUser.hasOwnProperty('role')
          && (currentUser.role === userRoles.manager.title || currentUser.role === userRoles.admin.title);
      },

      /**
       * Waits for currentUser to resolve before checking if user is logged in
       */
      isLoggedInAsync: function(cb)
			{
        if(currentUser.hasOwnProperty('$promise'))
				{
          currentUser.$promise.then(function()
					{
            return cb(true);
          })
						.catch(function(msg)
						{
							if (msg.data) FlashMessage.setMessage(msg.data);
							currentUser = getDefaultGuestProps();
							cb(false);
						});
        }
        else if(this.isLoggedIn())
				{
          cb(true);
        }
        else
				{
          currentUser = getDefaultGuestProps();
          cb(false);
        }
      },

      /**
       * Check if a user is an admin
       *
       * @return {Boolean}
       */
      isAdmin: function()
			{
        return currentUser.hasOwnProperty('role') && currentUser.role === userRoles.admin.title;
      }

    };

  }]);

