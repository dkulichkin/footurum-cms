'use strict';

angular.module('footurumCms')
  .factory('User', ['$resource', 'ApiEndpoint', function ($resource, ApiEndpoint)
  {
    return $resource(ApiEndpoint + '/users/:id', { id: '@_id' },
			{
				get: {
					method: 'GET',
					params: {
						id:'me'
					}
				}
			}
		);
	}]);
