'use strict';

/**
 * Simple service for flashMessage pattern
 */
angular.module('footurumCms')
  .factory('FlashMessage', [function()
  {
    var message = null;

    return {
      getMessage: function()
			{
        var oldVal = message;
        message = null;
        return oldVal;
      },
      setMessage: function(value)
			{
        message = value;
      }
    };
  }]);
