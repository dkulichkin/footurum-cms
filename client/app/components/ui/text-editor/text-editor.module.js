'use strict';

angular.module('textEditor', ['textAngular'])
	.config(['$provide', function($provide) {
		$provide.decorator('taOptions', ['$delegate', function(taOptions) {
			taOptions.toolbar = [
				['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
				['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
				['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
				['html', 'insertImage','insertLink']
			];
			return taOptions;
		}]);
	}]);