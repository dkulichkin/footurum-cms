'use strict';

angular.module('footurumCms')
	.controller('HeaderCtrl', ['$scope', 'Auth', 'UserProfileModel', '$log',
		function ($scope, Auth, UserProfileModel, $log)
		{
			$scope.language = UserProfileModel.language;
			$scope.logout = Auth.logout;
			$scope.isAdmin = UserProfileModel.role === 'admin';

			$scope.$watch('language', function(newLang)
			{
				$log.info('new lang is %s', newLang);
			});

		}]);
