'use strict';

angular.module('footurumCms')
  .factory('Modal', ['$mdDialog', function ($mdDialog) {

    return {

			showConfirmationModal: function(obj)
			{
        var confirm = $mdDialog.confirm().title(obj.title).ok('Confirm').cancel('Cancel');
        confirm._options.clickOutsideToClose = false;
        return $mdDialog.show(confirm);
      },

      showModal: function(obj)
	    {
        return $mdDialog.show({
          controller: ['$scope', '$mdDialog', function($scope, $mdDialog)
					{
						$scope.data = obj.data;
						$scope.cancel = function()
						{
							$mdDialog.cancel();
						};
						$scope.submit = function(valid)
						{
							if(valid) $mdDialog.hide($scope.data);
						};
					}],
          templateUrl: obj.templateUrl,
          locals: {
			      data: obj.data || {}
          },
			    escapeToClose: true,
          clickOutsideToClose: false,
          focusOnOpen: true,
          parent: 'body'
        });
      },

      showAlertModal: function(title, content)
			{
				title = title || 'Error';

        var alert = $mdDialog.alert({
          title: title,
          content: content.message || content,
          ok: 'Close'
        });
        alert._options.clickOutsideToClose = false;
        $mdDialog.show(alert).finally(function()
				{
					alert = undefined;
				});
      }

    }

  }]);
