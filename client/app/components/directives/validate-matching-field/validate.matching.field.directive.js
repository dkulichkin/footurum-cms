'use strict';

angular.module('footurumCms')
	.directive('validateMatchingField', function()
	{
		return {
			require: 'ngModel',
			link: function (scope, elem, attrs, model)
			{
				if (!attrs.validateMatchingField)
				{
					console.error('validateMatchingField expects a model as an argument!');
					return;
				}
				scope.$watch(attrs.nxEqualEx, function (value)
				{
					if (model.$viewValue !== undefined && model.$viewValue !== '')
					{
						model.$setValidity('validateMatchingField', value === model.$viewValue);
					}
				});
				model.$parsers.push(function (value)
				{
					if (value === undefined || value === '')
					{
						model.$setValidity('validateMatchingField', true);
						return value;
					}
					var isValid = value === scope.$eval(attrs.validateMatchingField);
					model.$setValidity('validateMatchingField', isValid);
					return isValid ? value : undefined;
				});
			}
		};
	});
