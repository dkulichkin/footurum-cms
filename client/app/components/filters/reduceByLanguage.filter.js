/**
 * @description Filters a collection by language
 */

angular.module('footurumCms')
	.filter('reduceByLanguage', function() {
		return function(collection, language, categories) {
			return collection.reduce(function(results, item)
				{

					if (item.languages.indexOf(language) !== -1)
					{
						item.name = item.name && item.name[language] || "";
						item.description = item.description && item.description[language] || "";
						item.tags = item.tags && item.tags[language] || [];
						item.category = categories[ item.category_id ] && categories[ item.category_id ][language] || "";

						results.push(item);
					}
					return results;
				}, []);
		}
	});