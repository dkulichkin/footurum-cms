'use strict';

angular.module('footurumCms', [
	'ngMaterial',
	'ngResource',
	'ngSanitize',
	'ngAnimate',
	'ui.router',
	'ngMessages',
	'LocalStorageModule',
	'RolesMapModule',
	'angular-loading-bar',
	'config',
	'md.data.table',
	'textEditor',
	'ngFileUpload',
	'angular-sortable-view'
])

  .config([
    '$stateProvider',
    '$urlRouterProvider',
    '$locationProvider',
    '$httpProvider',
    'localStorageServiceProvider',
    'rolesMapServiceProvider',
    '$compileProvider',
    function (
			$stateProvider,
			$urlRouterProvider,
			$locationProvider,
			$httpProvider,
			localStorageServiceProvider,
			rolesMapServiceProvider,
			$compileProvider
		)
		{
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(http?|ftp|mailto|chrome-extension|itms-services):/);

      $urlRouterProvider.rule(function($injector, $location)
      {
        var path = $location.path();
        if (path != '/' && path.slice(-1) === '/') $location.replace().path(path.slice(0, -1));
      });

      $urlRouterProvider.otherwise("/");

      $locationProvider.html5Mode(true);

      localStorageServiceProvider.setStorageType('localStorage').setNotify(true, true);

      $httpProvider.interceptors.push('authInterceptor');
  }])

  .run(['$rootScope', '$state', 'Auth', function ($rootScope, $state, Auth)
	{

    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function (event, toState)
		{

      if(!('data' in toState) || !('access' in toState.data))
			{
        // Access undefined for this state
        event.preventDefault();
      }
      else
			{
        Auth.isLoggedInAsync(function(loggedIn)
				{
          if (!Auth.authorize(toState.data.access))
					{
            // Seems like you tried accessing a route you don't have access to...
            event.preventDefault();

            // Redirecting depending on status
            if (loggedIn)
						{
              // User landing page
              $state.go('root.manager.skills');
            }
            else
						{
              $state.go('root.public.login');
            }
          }
        });
      }

    });

	$rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
		console.error('[stateChangeError]');
		console.error(error);
	});

  }]);
